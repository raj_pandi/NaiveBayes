#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 18:44:19 2017

@author: Dominic, Damion and Raj
"""

#!/usr/local/bin/python3
import pandas as pd
import numpy as np
import scipy
import scipy.sparse
import csv


def naive_bayes(likelihood, ma_p):
    ''' Y^new = argmax[ log_2(P(Y_x)) + \Sigma_i(# of X_i^new) log_2(P(X_i|Y_k))]
    '''
    #test_df  = pd.read_csv(filepath_or_buffer='testing.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=1)
    test_matrix = scipy.sparse.load_npz('./1ktesting.npz')
    #convert back to dataframe
    print("Matrix loaded and converting to DataFrame\n")
    test_df = pd.DataFrame( test_matrix.todense())
    #t_df = test_df.iloc[1,1:61189].values
    #log_map = np.log2(ma_p)
    #t_df = t_df.reshape(1,61188)
    #print(t_df.shape)
    #l_map = log_map.T
    #classifier = t_df.dot(l_map)
    #classify_naive = np.log2(likelihood) + classifier
    #print(np.argmax(classify_naive))
    prediction_list = []
    print("Test data length %i\n" % len(test_df[0]))
    with open('confusionresults.csv', 'w') as csvfile:
         resultwriter = csv.writer(csvfile, delimiter=',')
         resultwriter.writerow(['id','class'])
         for i_new in range(len(test_df[0])):
           log_map = np.log2(ma_p) 
           t_df = test_df.iloc[i_new,1:61189].values
           t_df = t_df.reshape(1,61188)
           l_map = log_map.T
           classifier = t_df.dot(l_map) 
           classify_naive = np.log2(likelihood) + classifier 
           article_number = np.argmax(classify_naive) + 1
           prediction_list.append(np.argmax(classify_naive))
           #print ("The prediction for document {} in test data is  {}".format(test_df.iloc[i_new,0],article_number)) 
           resultwriter.writerow([test_df.iloc[i_new,0],article_number])
    #print(prediction_list)
    #df = pd.DataFrame(prediction_list)

    #read_test = pd.read_csv('/home/rajkumar/cs529fall17/testing.csv')
    #read_test[61190] = df
    #read_test.to_csv('testingv1.csv',index=False)
    


def ma_p(df):
    '''P(X_i|Y_k) = ((count of X_i in Y_k) + (\Beta - 1)) / 
                    ((total words in Y_k) + ((\beta -1)*length of vocab))
    '''
    vocab_count = len(df.loc[1])-2
    ma_p = np.zeros(shape=(20,vocab_count))
    beta = 1+(1/vocab_count)
    #beta = 1 + .011125
    for doc_num in range(1,21):
        total_words_in_Yk = df[df[61189]==doc_num].iloc[:,1:61189].sum().sum()
        denominator = total_words_in_Yk + ((beta-1) * vocab_count)
        ma_p[doc_num-1] = (df[df[61189]==doc_num].iloc[:,1:61189].sum() + beta - 1)/denominator
    return ma_p
    
def mle(df):
    '''P(Y_k) = # of docs labled Y_k / total # of docs
    '''
    mle_list = []
    #total number of documents 100
    total_doc=len(df[61188])
    #Prior_PY_MLE = pd.Series(( for i in range(1,21): len(df.loc[df[61189]==i,1:61188])/total_doc))
    for i in range(1,21):
        #df_filtered = df[df[61189]==i]
        #mle.append(len(df_filtered)/total_doc)
        mle_list.append(len(df.loc[df[61189]==i,1:61188])/total_doc)
    likelihood = np.array(mle_list)
    return likelihood

def main():
    #Now you can load your matrix at any time:
    s_matrix = scipy.sparse.load_npz('./11ktraining.npz')
    #convert back to dataframe
    print("Matrix loaded and converting to DataFrame\n")
    df = pd.DataFrame( s_matrix.todense())



    #df = pd.read_csv(filepath_or_buffer='../../training.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=50)
    print("DataFrame loaded and running\n")
    likelihood = mle(df)
    #print(likelihood)
    ma_p1 = ma_p(df)
    #print(ma_p1)
    naive_bayes(likelihood,ma_p1)
    #s_matrix = scipy.sparse.csr_matrix(df.values)


if __name__ == "__main__": 
    main()     
