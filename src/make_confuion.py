#!/usr/local/bin/python3

import pandas as pd
import numpy as np
import scipy
import matplotlib.pyplot as plt
from pandas_ml import ConfusionMatrix



def confusion_matrix(actual_results,predicted_results):
  cm = ConfusionMatrix(actual_results,predicted_results)
  cm.plot()
  plt.savefig('confusion_matrix.png')
  return



def main():
    df_actual = pd.read_csv(filepath_or_buffer='valid1ktest.csv',sep=',',na_values='')
    df_predicted = pd.read_csv(filepath_or_buffer='confusionresults.csv',sep=',',na_values='')
    s_actual = pd.Series(df_actual['class'],name='Actual')
    s_predicted = pd.Series(df_predicted['class'],name='Predicted')
    df_confusion = pd.crosstab(s_actual, s_predicted, rownames=['Actual'], colnames=['Predicted'], margins=True)

    pd.options.display.max_columns = 50
    pd.set_option('expand_frame_repr', False)
    print(df_confusion)
    
    confusion_matrix(s_actual,s_predicted)


if __name__ == "__main__":
            main()
