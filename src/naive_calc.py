# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 15:53:23 2017

@author: Dominic, Damion and Raj
"""

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import csc_matrix
from scipy import sparse
import itertools
from numpy import matrix, asmatrix
from array import array
"Vocabulary 61187"
V = 61187

class nb:
    #Takes a sparse matrix that is 20 by 61187
    def calc_sum(self, train):
            #sets matrix as part of class note I do not want this to change inside the class only in the functions"
            self.train = train
            global sumClass
            sumClass = []
            #sums the class by going down the 20 rows and summing each row"
            for i in range(20):
                #Ask about summing rows from 1 to 61187"
                sumClass.append(np.sum(self.train[i],axis = 1))
            #Transpose the row of sums so it can be used in the computation of the Naive bayes"
            sumClass = np.transpose(sumClass)
            
    def navieBay(self,test): 
        #Since the line passed will be passed as a sparse vector this extracts the cornates where the words are"
        A = sparse.find(test)
        b = A[1]
        for i in range(np.size(b)):
            #Since train is a global variable this will only manipulate in the function right?"
            #Train needs to be change for the purpose of the computation but remain the same"
            #the next time the function is called"
            #add i to each the column i for each row in train this is xi + k in the algorithm"
            self.train[:b[i]] = self.train[:b[i] + i
            j = i * V
            sumClass = sumClass + j
            #finally to the division part of the algorithm and store it as a log"
            self.train = np.log(self.train[:i]/sumClass)
        #sum each row of the matrix since they are logs addition is equal to multiplying"
        prob = sum(self.train,axis=1)
        #finally return the max which is the most likely newsgroup that fits that article"
        #I cant figure out how to get the index of the max probability because thats what we want"
        return np.max(prob)