#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 18:44:19 2017

@author: Dominic, Damion and Raj
"""

#!/usr/local/bin/python3
import timeit
import pandas as pd
import numpy as np
import scipy
import scipy.sparse
import csv
import re
import sys




# naive_bayes function
# Takes: likelihood matrix, MAP matrix, test DataFrame, and results filename
# Use training matrices to calculate results from testing data.
# Results are written to csv file name generated above
def naive_bayes(likelihood, ma_p, test_df, results_out_file):
    ''' Y^new = argmax[ log_2(P(Y_x)) + \Sigma_i(# of X_i^new) log_2(P(X_i|Y_k))]
    '''
    prediction_list = []
    with open(results_out_file, 'w') as csvfile:
         resultwriter = csv.writer(csvfile, delimiter=',')
         resultwriter.writerow(['id','class'])
         for i_new in range(len(test_df[0])):
           log_map = np.log2(ma_p) 
           t_df = test_df.iloc[i_new,1:61189].values
           t_df = t_df.reshape(1,61188)
           l_map = log_map.T
           classifier = t_df.dot(l_map) 
           classify_naive = np.log2(likelihood) + classifier 
           article_number = np.argmax(classify_naive) + 1
           #prediction_list.append(np.argmax(classify_naive))
           resultwriter.writerow([test_df.iloc[i_new,0],article_number])
    print("Results writen to: %s" % results_out_file)
    #return prediction_list

    
def ent_p(ma_p1):
    data = [line.strip() for line in open("vocabulary.txt", 'r')]
    entropy_matrix = ma_p1 * np.log2(ma_p1)
    entropy_sum = np.sum(entropy_matrix, axis =0)
    entropy_sum = entropy_sum.T
    k = -1
    mult1 = k*entropy_sum
    indexs =list( range(0,61188))
    vocab_dict =  dict(zip(data,indexs))
    minimum_100 = mult1.argsort()[:100]
    lowScore_words = []
    for i in minimum_100:
        for key, value in vocab_dict.items():
            if value == i:
               lowScore_words.append(key)
    print('The first 100 words frequently appears with our best accuracy :\n{}'.format(lowScore_words))

    
# ma_p function
# Calculates Maximum a Posteriori (MAP) 
# Takes: total training DataFrame
# Returns: MAP matrix 20 x size_of(Vocabulary)
def ma_p(df):
    '''P(X_i|Y_k) = ((count of X_i in Y_k) + (\Beta - 1)) / 
                    ((total words in Y_k) + ((\Beta -1)*length of vocab))
    '''
    vocab_count = len(df.loc[1])-2
    ma_p = np.zeros(shape=(20,vocab_count))
    #beta = 1+(1/vocab_count)
    beta = 1 + .011125
    
    for doc_num in range(1,21):
        total_words_in_Yk = df[df[61189]==doc_num].iloc[:,1:61189].sum().sum()
        denominator = total_words_in_Yk + ((beta-1) * vocab_count)
        ma_p[doc_num-1] = (df[df[61189]==doc_num].iloc[:,1:61189].sum() + beta - 1)/denominator

    return ma_p
    

# mle function
# Calculates Maximum Likelihood Estimate matrix
# Takes: total training DataFrame
# Returns: MLE matrix 20 x size_of(Vocabulary)
def mle(df):
    '''P(Y_k) = # of docs labled Y_k / total # of docs
    '''
    mle_list = []
    total_doc=len(df[61188])

    for i in range(1,21):
          mle_list.append(len(df.loc[df[61189]==i,1:61188])/total_doc)
    likelihood = np.array(mle_list)
    return likelihood



def main():

    # Out of curriosity, how long does this run
    start = timeit.default_timer()


    # During development we generated compressed sparse matrix npz files to 
    # quickly load into memory.  These are default files to load
    training_file = './training_spmatrix.npz'
    testing_file = './testing.npz'

    # This provides for input file format based on extension
    ftype_npz = re.compile('.*\.npz$')
    ftype_csv = re.compile('.*\.csv$')

    # Check for command line args for file names
    # Usage: %s <training_data.[csv|npz]>   <testing_data.[csv|npz]>
    if len(sys.argv) > 1:
       training_file = sys.argv[1]
    if len(sys.argv) > 2:
       testing_file = sys.argv[2]


    # Generate results output file based on testing data input name
    # Writes to current working directory <testingfile>_results.csv
    results_out_file = re.search('(?=.*)\w+(?=.csv|.npz)',testing_file).group()
    results_out_file = './' + results_out_file + '_results.csv'

    # Print out script usage 
    print("\nUsage: %s <training_data.[csv|npz]>   <testing_data.[csv|npz]>\n" % sys.argv[0])

    # Print out Training, Testing, and Results file names
    print("Training Data:  %s \nTesting Data: %s" % (training_file,testing_file))
    print("Results file: %s" % results_out_file)

    # Request confirmation of correct filenames and proceed. Else exit program 
    AnsChar = input("Continue (Y/N)? ")
    if (AnsChar == 'Y') or (AnsChar == 'y'):
        print("\nLoading DataFrames for Training and Testing Data")
    else:
        exit()


    # Load training data from compressed sparse matrix npz file and
    # convert back to dataframe or load dataframe direct from CSV. 
    # Relies on python file loaders to complain of bad file names.
    if ftype_npz.match(training_file):
       s_matrix = scipy.sparse.load_npz(training_file)
       df = pd.DataFrame( s_matrix.todense())
    elif ftype_csv.match(training_file):
       df = pd.read_csv(filepath_or_buffer=training_file,sep=',',na_values='',header=None)
    else:
        print("Bad file name: %s" % training_file)
        exit()


    # Load training data from compressed sparse matrix npz file and
    # convert back to dataframe or load dataframe direct from CSV. 
    # Relies on python file loaders to complain of bad file names.
    if ftype_npz.match(testing_file): 
        test_matrix = scipy.sparse.load_npz(testing_file) 
        test_df = pd.DataFrame( test_matrix.todense())
    elif ftype_csv.match(testing_file): 
        test_df = pd.read_csv(filepath_or_buffer=testing_file,sep=',',na_values='',header=None)
    else:
        print("Bad file name: %s" % testing_file)
        exit()


    print("\nDataFrames loaded and running\n")

    # mle function
    # Calculates Maximum Likelihood Estimate matrix
    # Takes: total training DataFrame
    # Returns: MLE matrix 20 x size_of(Vocabulary)
    likelihood = mle(df)

    # ma_p function
    # Calculates Maximum a Posteriori (MAP) 
    # Takes: total training DataFrame
    # Returns: MAP matrix 20 x size_of(Vocabulary)
    ma_p1 = ma_p(df)


    # naive_bayes function
    # Takes: likelihood matrix, MAP matrix, test DataFrame, and results filename
    # Use training matrices to calculate results from testing data.
    # Results are written to csv file name generated above
    naive_bayes(likelihood,ma_p1,test_df,results_out_file)

    print("\nMining for frequent words starts.....\n")
    ent_p(ma_p1)


    stop =  timeit.default_timer() 
    print("\nThe overall run time of program", stop-start)


if __name__ == "__main__": 
        main()     

