#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 18:44:19 2017

@author: Dominic, Damion and Raj
"""

#!/usr/local/bin/python3

import pandas as pd
<<<<<<< HEAD:src/nb.bak.2.py
=======
import timeit
from termcolor import colored,cprint
from pandas_ml import ConfusionMatrix
>>>>>>> 7aae61fd0a62f01ce1316ba4df35900a42cfd843:src/nb.py
import numpy as np
import scipy
import scipy.sparse
import csv

<<<<<<< HEAD:src/nb.bak.2.py


def naive_bayes(likelihood, ma_p):
    ''' Y^new = argmax[ log_2(P(Y_x)) + \Sigma_i(# of X_i^new) log_2(P(X_i|Y_k))]
    '''
    #test_df  = pd.read_csv(filepath_or_buffer='testing.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=1)
    test_matrix = scipy.sparse.load_npz('./testing.npz')
    #convert back to dataframe
    print("Matrix loaded and converting to DataFrame\n")
    test_df = pd.DataFrame( test_matrix.todense())
    #t_df = test_df.iloc[1,1:61189].values
    #log_map = np.log2(ma_p)
    #t_df = t_df.reshape(1,61188)
    #print(t_df.shape)
    #l_map = log_map.T
    #classifier = t_df.dot(l_map)
    #classify_naive = np.log2(likelihood) + classifier
    #print(np.argmax(classify_naive))
    prediction_list = []
    print("Test data length %i\n" % len(test_df[0]))
    with open('results.csv', 'w') as csvfile:
         resultwriter = csv.writer(csvfile, delimiter=',')
         resultwriter.writerow(['id','class'])
         for i_new in range(len(test_df[0])):
           log_map = np.log2(ma_p) 
           t_df = test_df.iloc[i_new,1:61189].values
           t_df = t_df.reshape(1,61188)
           l_map = log_map.T
           classifier = t_df.dot(l_map) 
           classify_naive = np.log2(likelihood) + classifier 
           article_number = np.argmax(classify_naive) + 1
           prediction_list.append(np.argmax(classify_naive))
           #print ("The prediction for document {} in test data is  {}".format(test_df.iloc[i_new,0],article_number)) 
           resultwriter.writerow([test_df.iloc[i_new,0],article_number])
    #print(prediction_list)
    #df = pd.DataFrame(prediction_list)
=======
"""def confuson_matrix():
  df_actual = pd.read_csv(filepath_or_buffer='valid1ktest.csv',sep=',',na_values='')
  df_predicted = pd.read_csv(filepath_or_buffer='confusionresults.csv',sep=',',na_values='')
  y_actu = pd.Series(df_actual['class'],name='Actual')
  y_pred = pd.Series(df_predicted['class'],name='Predicted')
  df_confusion = pd.crosstab(y_actu, y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)
  cm = ConfusionMatrix(y_actu,y_pred)
  cm.plot()
  plt.savefig('confusion_matrix.png')"""

def naive_bayes(likelihood, ma_p):
  ''' Y^new = argmax[ log_2(P(Y_x)) + \Sigma_i(# of X_i^new) log_2(P(X_i|Y_k))]'''
  test_matrix = scipy.sparse.load_npz('./testing.npz')
  print("Test data loaded....")
  test_df = pd.DataFrame( test_matrix.todense())
  prediction_list = []
  print("Test data length %i" % len(test_df[0]))
  with open('results.csv', 'w') as csvfile:
    resultwriter = csv.writer(csvfile, delimiter=',')
    resultwriter.writerow(['id','class'])
    for i_new in range(len(test_df[0])):
      log_map = np.log2(ma_p) 
      t_df = test_df.iloc[i_new,1:61189].values
      t_df = t_df.reshape(1,61188)
      l_map = log_map.T
      classifier = t_df.dot(l_map) 
      classify_naive = np.log2(likelihood) + classifier 
      article_number = np.argmax(classify_naive) + 1
      prediction_list.append(np.argmax(classify_naive))
      resultwriter.writerow([test_df.iloc[i_new,0],article_number])
  return prediction_list
>>>>>>> 7aae61fd0a62f01ce1316ba4df35900a42cfd843:src/nb.py

    #read_test = pd.read_csv('/home/rajkumar/cs529fall17/testing.csv')
    #read_test[61190] = df
    #read_test.to_csv('testingv1.csv',index=False)
    
def ent_p(ma_p1):
  data = [line.strip() for line in open("vocabulary.txt", 'r')]
  entropy_matrix = ma_p1 * np.log2(ma_p1)
  entropy_sum = np.sum(entropy_matrix, axis =0)
  entropy_sum = entropy_sum.T
  k = -1
  mult1 = k*entropy_sum
  indexs =list( range(0,61188))
  vocab_dict =  dict(zip(data,indexs))
  minimum_100 = mult1.argsort()[:100]
  lowScore_words = []
  for i in minimum_100:
    for key, value in vocab_dict.items():
      if value == i:
          lowScore_words.append(key)
  cprint('The first 100 words frequently appears with our best accuracy :\n{}'.format(lowScore_words),'green')


def ma_p(df):
    '''P(X_i|Y_k) = ((count of X_i in Y_k) + (\Beta - 1)) / 
                    ((total words in Y_k) + ((\beta -1)*length of vocab))
    '''
    vocab_count = len(df.loc[1])-2
    ma_p = np.zeros(shape=(20,vocab_count))
    #beta = 1+(1/vocab_count)
    beta = 1 + .011125
    
    for doc_num in range(1,21):
        total_words_in_Yk = df[df[61189]==doc_num].iloc[:,1:61189].sum().sum()
        denominator = total_words_in_Yk + ((beta-1) * vocab_count)
        ma_p[doc_num-1] = (df[df[61189]==doc_num].iloc[:,1:61189].sum() + beta - 1)/denominator
    return ma_p
    
def mle(df):
    '''P(Y_k) = # of docs labled Y_k / total # of docs
    '''
    mle_list = []
    #total number of documents 100
    total_doc=len(df[61188])
    #Prior_PY_MLE = pd.Series(( for i in range(1,21): len(df.loc[df[61189]==i,1:61188])/total_doc))
    for i in range(1,21):
        #df_filtered = df[df[61189]==i]
        #mle.append(len(df_filtered)/total_doc)
        mle_list.append(len(df.loc[df[61189]==i,1:61188])/total_doc)
    likelihood = np.array(mle_list)
    return likelihood

def main():
    #Now you can load your matrix at any time:
    start = timeit.default_timer()
    s_matrix = scipy.sparse.load_npz('./training_spmatrix.npz')
    #convert back to dataframe
    cprint('Loading entire training dataset and converting to DataFrame....\n','green')
    df = pd.DataFrame( s_matrix.todense())

<<<<<<< HEAD:src/nb.bak.2.py


    #df = pd.read_csv(filepath_or_buffer='../../training.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=50)
    print("DataFrame loaded and running\n")
=======
    text = colored('Calculating MLE....','red',attrs=['reverse','blink'])
    print(text)
>>>>>>> 7aae61fd0a62f01ce1316ba4df35900a42cfd843:src/nb.py
    likelihood = mle(df)
    text2 = colored ("Finished MLE calculations",'green',attrs=['reverse','blink'])
    print(text2)
    text3 = colored ('Calculating MAP ....','red',attrs=['reverse','blink'])
    print(text3)
    ma_p1 = ma_p(df)
<<<<<<< HEAD:src/nb.bak.2.py
    ent_p1 = ent_p(ma_p1)
    naive_bayes(likelihood,ma_p1)
    #s_matrix = scipy.sparse.csr_matrix(df.values)
=======
    text4 = colored("Finished MAP calculations\n",'green',attrs=['reverse','blink'])
    print(text4)
    text5 = colored("Prediction starts ....",'red',attrs = ['reverse','blink'])
    print(text5)
    naive_bayes(likelihood,ma_p1)
    text6 = colored("Prediction done and saved as results.csv for Kaggle upload\n",'green',attrs = ['reverse','blink'])
    print(text6)
    cprint("Mining for frequent words starts.....\n",'cyan')
    ent_p(ma_p1)
    #confuson_matrix()
    stop =  timeit.default_timer()
    print("The overall run time of program %d",stop-start)
>>>>>>> 7aae61fd0a62f01ce1316ba4df35900a42cfd843:src/nb.py


if __name__ == "__main__": 
    main()     
