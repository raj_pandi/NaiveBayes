<pre>
https://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
https://docs.scipy.org/doc/scipy/reference/sparse.html
https://pandas.pydata.org/pandas-docs/stable/dsintro.html
https://stackoverflow.com/questions/20799593/reading-csv-containing-a-list-in-pandas
https://tomaugspurger.github.io/modern-4-performance.html
https://sites.google.com/site/kittipat/programming-with-python/importlargefilesizeintopythonusingpandas
https://pandas.pydata.org/pandas-docs/stable/sparse.html





Breakup training by class

mkdir data
for i in seq 1 20; do egrep ",${i}$" training.csv >> data/${i}.training.csv; done


wc -l *
     483 1.training.csv
     624 2.training.csv
     622 3.training.csv
     643 4.training.csv
     602 5.training.csv
     630 6.training.csv
     618 7.training.csv
     614 8.training.csv
     649 9.training.csv
     628 10.training.csv
     646 11.training.csv
     639 12.training.csv
     626 13.training.csv
     621 14.training.csv
     637 15.training.csv
     651 16.training.csv
     580 17.training.csv
     593 18.training.csv
     467 19.training.csv
     427 20.training.csv
   12000 total
   
   
   Header file
cat vocabulary.txt |tr '\n' ',' >vocab2.txt
sed -ie 's/,$//' vocab2.txt

In Python3:


#read in csv to dataframe
df = pd.read_csv(filepath_or_buffer='../training.csv',sep=',',na_values='0',header=None,dtype=np.float16)

#convert to matrix
s_matrix = csr_matrix(df.values)

#save the sparse matrix to file
scipy.sparse.save_npz('./filename.npz', s_matrix)

#Now you can load your matrix at any time:
s_matrix = scipy.sparse.load_npz('./training_spmatrix.npz')

#convert back to dataframe
df = pd.DataFrame( s_matrix.todense())

#change zero's to NaN, not sure if this helps memory
ndf = df.replace(0,np.nan)

#The only thing I couldn't get working was to convert dataframe to SparseDataFrame









header_row = pd.read_csv(filepath_or_buffer='../data/vocab2.txt',sep=',')
header_row
Empty DataFrame
Columns: [id, archive, name, atheism, resources, alt, last, modified, december, version, atheist, addresses, of, organizations, usa, freedom, from, religion, foundation, darwin, fish, bumper, stickers, and, assorted, other, paraphernalia, are, available, the, in, us, write, to, ffrf, box, madison, wi, telephone, evolution, designs, sell, it, symbol, like, ones, christians, stick, on, their, cars, but, with, feet, word, written, inside, deluxe, moulded, plastic, is, postpaid, laurel, canyon, north, hollywood, ca, people, san, francisco, bay, area, can, get, lynn, gold, try, mailing, figmo, netcom, com, for, net, who, go, directly, price, per, american, press, aap, publish, various, books, critiques, bible, lists, biblical, contradictions, so, ...]
Index: []

[0 rows x 61190 columns]
tp = pd.read_csv(filepath_or_buffer='../data/1.training.csv',sep=',',na_values='0',header=None,names=header_row,iterator=True)
tp
<pandas.io.parsers.TextFileReader object at 0x120d9a240>
df = pd.concat(tp,ignore_index=True)
df
        id  archive  name  atheism  resources  alt  last  modified  december  \
0       14      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
1       37      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
2       63      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
3       65      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
4       80      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
5      117      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
6      153      NaN   NaN      NaN        NaN  NaN   NaN       NaN       NaN   
....
480      NaN   ...        NaN       NaN      NaN     NaN        NaN     NaN   
481      NaN   ...        NaN       NaN      NaN     NaN        NaN     NaN   
482      NaN   ...        NaN       NaN      NaN     NaN        NaN     NaN   

     ephas  kltensme  etrbom  class.1  
0      NaN       NaN     NaN        1  
1      NaN       NaN     NaN        1  
2      NaN       NaN     NaN        1  
3      NaN       NaN     NaN        1  
...
480    NaN       NaN     NaN        1  
481    NaN       NaN     NaN        1  
482    NaN       NaN     NaN        1  

[483 rows x 61190 columns]


>>> np.argmax(df.loc[df[61189]==20,1:61188].mean())

