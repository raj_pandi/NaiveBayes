# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 15:53:23 2017

@author: Raj
"""

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse import csc_matrix
from scipy import sparse
import itertools
from numpy import matrix, asmatrix
from array import array
"Vocabulary 61188"
V = 61188
beta = 1.6343073805321303e-05
class nb:
    "Takes a sparse matrix that is 20 by 61188"
    def __init__(self, train):
            "sets matrix as part of class note I do not want this to change inside the class only in the functions"
            self.train = train
            """Sums the rows for a total word count of each class"""
            sumClass = np.sum(self.train,axis = 1)
            sumClass = sumClass + (beta * V)
            
            
    def navieBay(self,test): 
        "Since the line passed will be passed as a sparse vector this extracts the cornates where the words are"
        A = sparse.find(test)
        b = A[1]
        for i in range(np.size(b)):
            """Create temporary matrix for computation with the sums of the words that
            are in the test article for each class and add the beta value to it"""
            compM[:b[i]] = self.train[:b[i] + beta
            
            "calculate prop of each word and store as log"
            self.train = np.log(compM/sumClass)
        "sum each row of the matrix since they are logs addition is equal to multiplying"
        prob = sum(compM,axis=1)
        "finally return the max which is the most likely newsgroup that fits that article"
        
        "I cant figure out how to get the index of the max probability because thats what we want"
        return np.loc(np.max(prob))
            
            
    
