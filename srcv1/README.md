<pre>
PACKAGES_NEEDED
 scipy
 pandas
 pandas_nl
 matlplotlib
 numpy 
 sys
 re

FILE_LIST
 nb.py                -  'python script for Naive Bayes implementation'
 make_confusion.py    -  'python script for making confusion plots'
 training.npz         -  'required trainind dataset'
 testing.npz          -  'required testing dataset'
 confusion_matrix.png -  '20 *20 confusion matrix plot'
 11ktraining.npz      -  '11k training dataset'
 1ktesting.npz        -  '1k testing dataset'
 confusion_results.csv -  'dataset required for make_confusion.py'
 valid1ktest.csv      -  'dataset required for make_confusion.py'
 vocabulary.txt       -  '61188 vocabulary words'

RUN
nb.py:

Usage: ./nb.py <training_data.[csv|npz]>   <testing_data.[csv|npz]>

Training Data:  ./training_spmatrix.npz 
Testing Data: ./testing.npz
Results file: ./testing_results.csv
Continue (Y/N)? 

press 'Y', to continue running the program. The program finally outputs 'results.csv', printing 'top 100 words used by the classifier' to the console.


make_confusion.py:

Usage: make_confusion.py
The program is hard coded to load valid1ktest.csv and confusion_results.csv
The program saves confusion_matrix.png to the current directory.

