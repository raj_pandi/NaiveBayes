#!/usr/local/bin/python3

import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from scipy import sparse
import scipy as sp
import itertools
from naivebase import nb
#import os
#import matplotlib.pyplot as plt
#import seaborn as sns

#header_row = pd.read_csv(filepath_or_buffer='../data/vocab2.txt',sep=',')
#header_row
#tp = pd.read_csv(filepath_or_buffer='../data/1.training.csv',sep=',',na_values='0',header=None, names=header_row,iterator=True) #, chunksize=1)
#ndf = pd.read_csv(filepath_or_buffer='../training.csv',sep=',',na_values='0',header=None,dtype=np.float16)

s_matrix = scipy.sparse.load_npz('./training_spmatrix.npz')

df = pd.DataFrame( s_matrix.todense())
ndf = df.replace(0,np.nan)

DOCS=len(df[0])

# axis may be an issue
#df = pd.concat(tp,ignore_index=True)
#df = pd.concat(tp,axis=1,ignore_index=True)

#print(df)

#sumf = pd.DataFrame(np.nan, columns=range(len(df.loc[0])-1), index=range(20))
#sumf = csr_matrix((20,len(df.loc[0])-2))
sumf = scipy.sparse.load_npz('./vocab_summed_by_class.npz')

mything = nb()
mything.someotherfunction(sumf)
mything.naiveBay(

"""for j in range(1,21): 
    print("Row %i" % j)
    sumf[j-1] = ndf.loc[ndf[61189]==j,1:61188].sum(axis=0)"""
                

""" Prior P(Y) with MLE
    P(Y) = [y1/DOCS, ..., y20/DOCS] 

Prior_PY_MLE = pd.Series(( for i in range(1,21): len(df.loc[df[61189]==i,1:61188])/DOCS))
"""

"""
This calculates the sum of all the words in the class
df.loc[df[61189]==1,1:61188].sum().sum()
154382.0

"""

"""  B = 1/|V|    where V is # of docs in Class

     P(x1|y1) = (0+B)/(n+mk) =  """
