# -*- coding: utf-8 -*-
"""
Created on Sun Oct 15 18:44:19 2017

@author: Dominic, Damion and Raj
"""

#!/usr/local/bin/python3
import mmap
import os
import pandas as pd
import numpy as np
import scipy


def naive_bayes(likelihood, ma_p):
    test_df  = pd.read_csv(filepath_or_buffer='testing.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=1)
    t_df1 = test_df.iloc[:,1:61189]
    print (type(t_df1))
    log_likelihood = np.log2(likelihood)
    log_map = np.log2(ma_p)
    classifier = np.dot(t_df1,log_map)
    
    classify_naive = log_likelihood + classifier
    print(classify_naive)
    
def ma_p(df):
    ma_p = np.zeros(shape=(20,61188))
    beta = 1+(1/61188)
    for doc_num in range(1,21):
        df_filtered = df[df[61189]==doc_num] 
        df1 = df_filtered.iloc[:,1:61189]
        for i in range(1,61189):
            over_tot = df1.values.sum()
            pxy = df1[i].sum()
            numerator = pxy + (beta-1)
            denominator = over_tot + ((beta-1) * 61188)
            px_y = numerator/denominator
            ma_p[doc_num-1][i-1 ] = px_y
    return ma_p
    
def mle(df):
    mle = []
    #total number of documents 100
    total_doc =50
    for i in range(20):
        df_filtered = df[df[61189]==i]
        mle.append(len(df_filtered)/total_doc)
    likelihood = np.array(mle)
    return likelihood


def main():
    df = pd.read_csv('training.csv',sep=',',na_values='',header=None,dtype=np.float16,nrows=10)
    likelihood = mle(df)
    ma_p1 = ma_p(df)
    naive_bayes(likelihood,ma_p1)
    #s_matrix = scipy.sparse.csr_matrix(df.values)


if __name__ == "__main__": 
    main()     

