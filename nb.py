# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 16:38:37 2017

@author: Dominic, Damion and Raj
"""
import pandas as pd
import numpy as np
import scipy as scipy
import glob
from scipy.sparse import csr_matrix
 


def main():
    """use the splitted train data and convert into csr_matrix
    for train_data in glob.glob("../data/*.csv"):
        df = pd.read_csv(train_data)
        s_matrix = csr_matrix(df.values)
        print (s_matrix)  #use s_matrix as a function argument
    """

    """Use the following to load the total training data from 
       compressed sparse matrix
    """ 
    """smz_fh = np.load('./training_spcompmatrix.npz') """

    s_matrix = scipy.sparse.load_npz('./training_spmatrix.npz')


if __name__ == "__main__":
    main()
